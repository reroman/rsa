/*
 * rsa.h
 * 
 * Copyright 2014 Ricardo Román <reroman4@gmail.com>
 * 
 */

#pragma once

#ifndef RSA_H
#define RSA_H

#include <stdint.h>

typedef struct{
	uint64_t e;
	uint64_t d;
	uint64_t n;
} RSAKeys;

/**
 * @name: random_bits
 * Genera un número aleatorio de n bits.
 *
 * @param: n es el número de bits del número a generar.
 *
 * @return: El número generado.
 */
uint64_t random_bits( int n );

/**
 * @name: createKeys
 * Crea los valores para las llaves de cifrado y
 * descifrado del algoritmo RSA.
 *
 * @param: n indica el número de bits del módulo.
 *
 * @return: Una estructura que contiene las claves de
 * cifrado y descifrado
 */
RSAKeys createKeys( int n );

/**
 * @name: encrypt
 * Cifra un número c, utilizando el algoritmo RSA.
 *
 * @param: c es el valor a cifrar, el apuntador keys contiene
 * las llaves con las que se hará el cifrado.
 *
 * @return: El valor c cifrado.
 */
uint64_t encrypt( uint64_t c, const RSAKeys *keys );

/**
 * @name: decrypt
 * Descifra un número, utilizando el algoritmo RSA.
 *
 * @param: c es el valor a descifrar, el apuntador keys contiene
 * las llaves con las que se hará el descifrado.
 *
 * @return: El valor c descifrado.
 */
uint64_t decrypt( uint64_t c, const RSAKeys *keys );

/**
 * @name: bin_pow_mod
 * Calcula la potencia de un número y obtiene el módulo. El
 * algoritmo utiliza la exponenciación binaria en conjunto con 
 * aritmética modular.
 *
 * @param: a indica la base, x el exponente y n el módulo.
 *
 * @return: El resultado de a^x mod n.
 */
uint64_t bin_pow_mod( uint64_t a, uint64_t x, uint64_t n );

/**
 * @name: gcd
 * Obtiene el máximo común divisor de dos números. La función
 * utiliza el algoritmo de euclides.
 *
 * @param: a y b: los números a calcular su MCD.
 *
 * @return: El MCD entre a y b.
 */
uint64_t gcd( uint64_t a, uint64_t b );

/**
 * @name: invert
 * Obtiene el multiplicador modular inverso de 2 números,
 * utilizando el algoritmo de Euclides extendido.
 *
 * @param: a y b: los números de los cuales se calculará
 * su multiplicador modular inverso.
 *
 * @return: n, tal que n*a ≡ 1 mod b
 */
uint64_t invert( uint64_t a, uint64_t b );

/**
 * @name: isPrime
 * Determina si un número es primo mediante un sencillo
 * test de primalidad.
 *
 * @param: a, el número a evaluar.
 *
 * @return: 1, si a es primo; 0 en caso contrario.
 */
int isPrime( uint64_t a );

/**
 * @name nextPrime
 * Retorna el siguiente número primo de un determinado
 * número
 *
 * @param: a, indica a partir de qué número obtener el 
 * siquiente primo.
 *
 * @return: El siguiente número primo.
 */
uint64_t nextPrime( uint64_t a );

#endif
