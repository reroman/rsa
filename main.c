/*

               					RSA 
				Programa de ejemplo para el algoritmo RSA
				   utilizando claves de de 32 bits

		   Copyright 2014 Ricardo Román <reroman4@gmail.com>
               ----------------------------------------

*/

#include <stdio.h>
#include <string.h>
#include "rsa.h"

#if defined(_WIN32) || defined(_WIN64)
// Aunque no es correcto limpiar el buffer de entrada con
// fflush, he visto que funciona en Windows. Además no sé
// si cuente con la función __fpurge()
#define clear_stdin()	fflush( stdin )

#else
#include <stdio_ext.h>
#define clear_stdin()	__fpurge( stdin )

#endif

// Indica el tamaño de la llave. Se establece en 32
// para evitar overflows en los tipos primitivos de C
#define KEY_SIZE			32

// Indica el número de bytes a cifrar de manera conjunta.
// Mientras más bytes, más rápido es el proceso, pero produciendo
// resultados más grandes (más de 3 bytes produce overflows y 
// podría no cifrar/descifrar correctamente)
#define BYTES_TOGETHER		3


/**
 * @name: readline
 * Lee como máximo n-1 caracteres desde un flujo de
 * entrada. La lectura finaliza después de un salto de línea
 * o un fin de archivo. El salto de línea es descartado y el
 * caracter de fin de cadena es agregado.
 *
 * @param: buf es el apuntador donde se almacenará el contenido
 * leído. n es el tamaño del buffer. Y stream representa el flujo
 * de entrada.
 *
 * @return: El número de caracteres leídos.
 */
int readline( char *buf, int n, FILE *stream )
{
	char *newLineChar;

	clear_stdin();
	fgets( buf, n, stream );
	
	newLineChar = strrchr( buf, '\n' );
	if( newLineChar )
		*newLineChar = '\0';

	return strlen( buf );
}

/**
 * @name: encryptFile
 * Cifra un archivo. Produce una salida con nombreArchivo.crypt
 * e imprime en pantalla en caso de error.
 *
 * @param: fileName es el nombre del archivo a cifrar; el 
 * apuntador keys almacena las llaves de cifrado.
 *
 * @return: 0 en caso de éxito. -1 si ocurrió algún error.
 */
int encryptFile( const char *fileName, const RSAKeys *keys )
{
	FILE *fileIn = fopen( fileName, "rb" );
	FILE *fileOut;
	char fileOutName[BUFSIZ+5];
	uint32_t msg;
	uint64_t digest;

	if( !fileIn ){
		perror( fileName );
		return -1;
	}

	// Agrega la extensión .crypt al nombre de archivo y lo almacena
	// en fileOutName
	sprintf( fileOutName, "%s.crypt", fileName );
	if( (fileOut = fopen( fileOutName, "wb" ) ) == NULL ){
		perror( fileOutName );
		return -1;
	}
	
	while( fread( &msg, 1, BYTES_TOGETHER, fileIn ) > 0 ){
		digest = encrypt( msg, keys );
		fwrite( &digest, sizeof(uint64_t), 1, fileOut );
		msg = 0;
	}

	fclose( fileIn );
	fclose( fileOut );
	return 0;
}

/**
 * @name: decryptFile
 * Descifra un archivo. Produce una salida eliminando la
 * extensión .crypt del archivo, si no la tuviera se
 * agrega la extensión .txt. Además imprime en 
 * pantalla si ocurrieron errores.
 *
 * @param: fileName es el nombre del archivo a descifrar. La cadena
 * se modifica con el nombre del archivo de salida; keys contiene
 * las llaves de descifrado.
 *
 * @return: 0 en caso de éxito; -1 en caso de error.
 */
int decryptFile( char *fileName, const RSAKeys *keys )
{
	FILE *fileIn = fopen( fileName, "rb" );
	FILE *fileOut;
	char *fileExt;
	uint64_t digest;
	uint32_t msg;

	if( !fileIn ){
		perror( fileName );
		return -1;
	}
	
	// Elimina la extensión .crypt. Si no la tuviera
	// simplemente agrega la extensión de salida .txt.
	fileExt = strstr( fileName, ".crypt" );
	if( fileExt )
		*fileExt = '\0';
	else
		strcat( fileName, ".txt" );

	if( (fileOut = fopen( fileName, "wb" )) == NULL ){
		perror( fileName );
		return -1;
	}

	while( fread( &digest, sizeof(uint64_t), 1, fileIn ) > 0 ){
		msg = decrypt( digest, keys );
		fwrite( &msg, 1, BYTES_TOGETHER, fileOut );
		digest = 0;
	}

	fclose( fileIn );
	fclose( fileOut );
	return 0;
}


int main()
{
	RSAKeys keys;
	char fileName[BUFSIZ];
	FILE *fileKeys;
	int op;

	// Intenta cargar las llaves desde el archivo keys.rsa
	// Si no existiera el archivo, lo crea junto con nuevas llaves.
	// Lo correcto sería separar la clave pública de la privada, pero bue...
	// esto es un ejemplo xD
	if( (fileKeys = fopen( "keys.rsa", "rb" ) ) == NULL ){
		keys = createKeys( KEY_SIZE );
		fileKeys = fopen( "keys.rsa", "wb" );
		fwrite( &keys, sizeof(RSAKeys), 1, fileKeys );
	}
	else
		fread( &keys, sizeof(RSAKeys), 1, fileKeys );
	fclose( fileKeys );

	printf( "SELECIONA UNA OPCIÓN:\n\n"
			"\t1) Cifrar archivo.\n"
			"\t2) Descifrar archivo.\n"
			"\tx) Salir.\n"
			"\nOpción: " );
	scanf( "%d", &op );

	if( op != 1 && op != 2 )
		return 0;

	printf( "Archivo? " );
	readline( fileName, BUFSIZ, stdin );
	
	if( op == 1 )
		return encryptFile( fileName, &keys );
	else
		return decryptFile( fileName, &keys );
}
