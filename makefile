#
# makefile
#
# Copyright 2014 Ricardo Román <reroman4@gmail.com>
#

CC := gcc
CFLAGS := -Wall -O3
LIBS := -lm
OBJS := main.o rsa.o
RM := rm -f

rsa: $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

main.o:

rsa.o: rsa.h

.PHONY: 
	clean

clean:
	$(RM) $(OBJS)
