# RSA #

Este es un ejemplo sencillo de la implementación del algoritmo de cifrado
asimétrico RSA en C aplicado a archivos. La implementación utiliza sólo datos
primitivos del lenguaje por lo que las llaves generadas son de 32 bits
solamente, el conveniente es que no necesita bibliotecas de terceros para poder
ser compilado.

**Nota:** Las claves consideradas seguras son de 1024 a 2048 bits, por lo 
que las claves de 32 bits no deben ser usadas para un proyecto real.

## Descripción del algoritmo ##

1. Se seleccionan 2 números primos de manera aleatoria *p* y *q*:

		p = 3
		q = 11
	***

2. Calculamos *n* y *Φ* tal que ```n = p * q``` y ```Φ = (p-1) * (q-1)```:

		n = 3 * 11 = 33
		Φ = (3-1) * (11-1) = 20
	***

3. Calculamos *e* tal que ```0 < e < Φ``` y sea coprimo con *Φ*, es decir, el 
máximo común divisor entre éstos sea 1.

	Algunos valores que cumplen lo anterior serían:

		3, 7, 9, 11, 13, 17, 19

	En nuestro caso escogeremos:

		e = 3
	***

4. Calculamos *d* tal que sea el multiplicador modular inverso entre *e* y *Φ*.
Esto se puede representar mediante la congruencia ```d*e ≡ 1 mod Φ```, esto a
su vez quiere decir que ```(d*e - 1) mod Φ = 0```.

	De igual forma tenemos varios posibles valores:

		7, 27, 47, 67, 87, ...

	Tomaremos:

		d = 7
	***

5. Para el cifrado de un mensaje se necesita tener la **clave pública (e, n)**
y una representación numérica del mensaje (p. ej. su código ASCII). Para este
ejemplo utilizaremos el siguiente código:

		A = 2
		B = 3
		C = 4
		 .
		 .
		 . 
		Z = 27

	Supongamos que queremos cifrar la letra *H* cuyo valor es el 9, para
	obtener el digesto haríamos:

		c = H^e mod n
		c = 9^3 mod 33
		c = 729 mod 33
		c = 3
	
	y *c* sería el mensaje cifrado.

	**\*Nota:** Hay que notar que un valor no puede ser la unidad puesto que el
	resultado de elevar 1 a cualquier potencia da siempre el mismo resultado.
	***

6. Para el descifrado, haríamos uso de la **clave privada (d, n)** haciendo lo
siguiente:

		m = c^d mod n
		m = 3^7 mod 33
		m = 2187 mod 33
		m = 9

	donde *m* sería el mensaje original y donde 9 es el código que corresponde
	a la letra *H*.


## Algoritmos utilizados ##

+ Exponenciación binaria
+ Algoritmo de Euclides
+ Algoritmo de Euclides extendido
+ Test de primalidad
+ Aritmética modular

## Archivos ##

:	**rsa.h:** Contiene los prototipos de funciones relacionadas con
el algoritmo. En él se explica para qué sirve cada función, qué
argumentos recibe y qué datos retorna.

:	**rsa.c:** Contiene la definición de cada una de las funciones
del algoritmo.

:	**main.c:** Contiene la función principal del programa y
algunas funciones auxiliares. De igual forma indican los argumentos
que recibe y valores de retorno.

:	**makefile:** Contiene las instrucciones necesarias para compilar el
proyecto.

## Compilación ##
Se puede utilizar el programa make (mingw32-make en Windows) para la
compilación o compilar manualmente los archivos con gcc.

> $make

o bien,

> $gcc -o rsa rsa.c main.c -lm

**Nota:** Es probable que se necesiten establecer las variables de entorno
para usar mingw desde cualquier ubicación en Windows.

## Contacto ##

Ricardo Román <[reroman4@gmail.com](mailto:reroman4@gmail.com)>
