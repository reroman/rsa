/*
 * rsa.c
 * 
 * Copyright 2014 Ricardo Román <reroman4@gmail.com>
 * 
 */

#include "rsa.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>

uint64_t random_bits( int n )
{
	static int first = 1;
	int i;
	char *num;
	uint64_t result;

	// Sólo se ejecuta la primera vez que se llama
	// a la función, esto para inicializar la semilla
	// de los números aleatorios
	if( first ){
		srand( time(NULL) );
		first = !first;
	}		

	num = malloc( n + 1 );
	num[0] = '1';	// Nos aseguramos que sea de n bits estableciendo el BMS en 1
	for( i = 1 ; i < n ; i++ )
		num[i] = rand() % 2 + '0';	// Genera aleatorios entre '0' y '1' (caracteres)
	num[n] = '\0';		// Se agrega el fin de cadena

	result = strtoull( num, NULL, 2 );
	free( num );

	return result;
}

RSAKeys createKeys( int n )
{
	/*
	 * Genera las llaves para el RSA. El algoritmo es:
	 * 1) Genera 2 números primos aleatorios p y q
	 * 2) Calcular n = pq y x = (p-1)(q-1)
	 * 3) Obtener e que sea coprimo con x. Normalmente es 65537.
	 * 4) Obtener d tal que es el multiplicador modular inverso entre e y x
	 */
	uint64_t p, q, x;
	int64_t d_aux;
	RSAKeys keys;

	do{
		p = nextPrime( random_bits( n/2 ) );
		q = nextPrime( random_bits( n/2 ) );

		keys.n = p * q;
		x = (p-1) * (q-1);

		keys.e = 65537;
		while( gcd( keys.e, x ) != 1 )
			keys.e += 2;

		d_aux = invert( keys.e, x );

	}while( d_aux <= 0 || keys.e >= x );

	keys.d = d_aux;
	return keys;
}

uint64_t encrypt( uint64_t c, const RSAKeys *keys )
{
	return bin_pow_mod( c, keys->e, keys->n );
}

uint64_t decrypt( uint64_t c, const RSAKeys *keys )
{
	return bin_pow_mod( c, keys->d, keys->n );
}

uint64_t bin_pow_mod( uint64_t a, uint64_t x, uint64_t n )
{
	uint64_t aux;

	if( n == 1 )
		return 0;

	if( x == 0 )
		return 1 % n;

	if( x == 1 )
		return a % n;

	if( x % 2 == 0 ){
		aux = bin_pow_mod( a, x/2, n );
		return aux * aux % n;
	}
	return a * bin_pow_mod( a, x-1, n ) % n;
}

uint64_t gcd( uint64_t a, uint64_t b )
{
	uint64_t mod;

	// Se intercambian valores si a es menor
	if( a < b ){
		a = a ^ b;
		b = a ^ b;
		a = a ^ b;
	}

	while( b != 0 ){
		mod = a % b;
		a = b;
		b = mod;
	}
	return a;;
}

uint64_t invert( uint64_t a, uint64_t b )
{
	uint64_t x, x1, x2, y, y1, y2, q, r;

	if( a < b ){
		a = a ^ b;
		b = a ^ b;
		a = a ^ b;
	}

	q = r = x1 = y2 = 0;
	x2 = y1 = 1;

	while( b > 0 ){
		q = ( a / b );
		r = a - q * b;
		x = x2 - q * x1;
		y = y2 - q * y1;
		a = b;
		b = r;
		x2 = x1;
		x1 = x;
		y2 = y1;
		y1 = y;
	}
	return y2;
}

int isPrime( uint64_t a )
{
	uint64_t sqrt_a = (uint64_t) sqrt( a );
	uint64_t i;

	if( a == 2 )
		return 1;
	
	if( a % 2 == 0 )
		return 0;

	for( i = 3 ; i <= sqrt_a ; i += 2 )
		if( a % i == 0 )
			return 0;

	return 1;
}

uint64_t nextPrime( uint64_t a )
{
	do
		a++;
	while( !isPrime( a ) );
	return a;
}
